const APP_DISPLAY_NAME = "Nuke Image";
const APP_NAME = "nukeimage";
const APP_PACKAGE = "/Ted Mielczarek/nukeimage";
const APP_VERSION = "0.3";

const APP_JAR_FILE = "nukeimage.jar";
const APP_CONTENT_FOLDER = "content/nukeimage/";
const APP_LOCALE_FOLDER  = "locale/en-US/nukeimage/";

const APP_SUCCESS_MESSAGE = "New menuitems will appear on the image context menu.\n\n";


initInstall(APP_NAME, APP_PACKAGE, APP_VERSION);

var chromef;

if(confirm("Install this into the application directory?  (Cancel will install into your profile directory)"))
  chromef = getFolder("Chrome");
else
  chromef = getFolder("Profile", "chrome");

var err = addFile(APP_PACKAGE, APP_VERSION, APP_JAR_FILE, chromef, null);

if(err >= SUCCESS) { 
	var jar = getFolder(chromef, APP_JAR_FILE);
	registerChrome(CONTENT | DELAYED_CHROME, jar, APP_CONTENT_FOLDER);
	registerChrome(LOCALE  | DELAYED_CHROME, jar, APP_LOCALE_FOLDER);
	err = performInstall();
	if(err >= SUCCESS) {
		alert(APP_NAME + " " + APP_VERSION + " has been succesfully installed.\n"
			+APP_SUCCESS_MESSAGE
			+"Please restart your browser before continuing.");
	} else { 
		alert("Install failed. Error code:" + err);
		cancelInstall(err);
	}
} else { 
	alert("Failed to create " +APP_JAR_FILE +"\n"
		+"You probably don't have appropriate permissions \n"
		+"(write access to phoenix/chrome directory). \n"
		+"_____________________________\nError code:" + err);
	cancelInstall(err);
}
