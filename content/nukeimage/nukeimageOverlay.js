/* -*- Mode: Java; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*-
 *
 * Copyright (c) 2001-2002  Ted Mielczarek
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

var nukeimageMenuItems = new Array("nukeimage-do-nuke");

function nukeimageContext() {
  if(gContextMenu) {
    for(var i=0; i<nukeimageMenuItems.length; i++) {
      var menuitem = document.getElementById(nukeimageMenuItems[i]);
      if(menuitem)
        menuitem.hidden = !gContextMenu.onImage;
    }
  }
}

function nukeImage() {
  if(gContextMenu) {
    var img = gContextMenu.target;
    if(img) {
      // go away, image!
      img.style.display = "none";
    }
  }
}


function nukeimageInit() {
  var menu = document.getElementById("contentAreaContextMenu");
  menu.addEventListener("popupshowing",nukeimageContext,false);
}

// do the init on load
window.addEventListener("load", nukeimageInit, false); 
